package com.group.api.ctrl;

import java.util.*;

import com.group.api.mod.inventory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/inventory")

public class Inventroller {
    
    @GetMapping("/all")
    public List<inventory> getInventory(){
        // create a list of InventoryItems
        List<inventory> allItems = new ArrayList<>();
        allItems.add(new inventory(1,"Test Item 1",14,27));
        return allItems;
    }
}
