package com.group.api.mod;

public class inventory {
    private long id;
    private String desc;
    private long count;
    private long costItem;

    public inventory(long id, String desc, long count, long costItem){  
        this.id = id;
        this.desc = desc;
        this.count = count;
        this.costItem =  costItem;
    }

    public inventory(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getCostItem() {
        return costItem;
    }

    public void setCostItem(long costItem) {
        this.costItem = costItem;
    }

    
}
